﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PowerBIFams2Go.aspx.cs" Inherits="PBIWebApp.PowerBIFams2Go" EnableEventValidation="false"%>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <script type="text/javascript">

        //This code is for sample purposes only.

        //Configure IFrame for the Report after you have an Access Token. See Default.aspx.cs to learn how to get an Access Token
        var myTimer = 0;
        window.onload = function () {
            var el = document.getElementById("bEmbedReportAction");
            if ("" != document.getElementById('MainContent_accessToken').value) {
                var iframe = document.getElementById('iFrameEmbedReport');

                // To load a Report do the following:
                // Set the IFrame source to the EmbedUrl from the Get Reports operation
                iframe.src = '<%=this.embededURL%>';
                    //            iframe.src = document.getElementById('MainContent_ReportEmbedUrl').value;asp 

                    // Add an onload handler to submit the access token
                    iframe.onload = postActionLoadReport;
            }        
        };
        function oncheck() {
            if (document.getElementById('TimerHTML').checked) {
                myTimer= setInterval(updateEmbedReport, 120000);
            }
            else
            {
                clearInterval(myTimer);
            }
        };
            function updateEmbedReport() {
                // check if the embed url was selected

             
            var selectedvalue = control.options[control.selectedIndex].value;
            var embedUrl = selectedvalue;
            if ("" === embedUrl)
                return;

            // to load a report do the following:
            // 1: set the url
            // 2: add a onload handler to submit the auth token
            iframe = document.getElementById('iFrameEmbedReport');
            iframe.src = embedUrl;
            iframe.onload = postActionLoadReport;
        }

        // Post the access token to the IFrame
        function postActionLoadReport() {

            // Construct the push message structure
            // this structure also supports setting the reportId, groupId, height, and width.
            // when using a report in a group, you must provide the groupId on the iFrame SRC
            var m = {
                action: "loadReport",
                accessToken: document.getElementById('MainContent_accessToken').value
            };
            message = JSON.stringify(m);

            // push the message.
            iframe = document.getElementById('iFrameEmbedReport');
            iframe.contentWindow.postMessage(message, "*");;
        }
    </script>

    <asp:HiddenField ID="accessToken" runat="server" />

    <iframe id="iFrameEmbedReport" style="position: absolute; width: 99%; height: 98%;"></iframe>
    <br />
    

</asp:Content>