﻿using System;
using System.Web;
using System.Web.UI;
using System.Collections.Specialized;
using Newtonsoft.Json;
using PBIWebApp.Properties;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using System.Security.Authentication;
using System.Text;
using System.Net;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using System.IO;
using System.Security.Cryptography;
using System.Configuration;

namespace PBIWebApp
{
    public partial class PowerBiNewPortal : System.Web.UI.Page
    {
        DataSet ds;
        public string embededURL = "";
        string baseUri = Properties.Settings.Default.PowerBiDataset;
        string safe;
        public static string accountid,dash,userID;
        public string conString;


        //SQL Commands
        string sqlPowerBiCredentials = "SELECT PowerBi_Credentials.Description ,PowerBi_Credentials.Username ,PowerBi_Credentials.Password ,PowerBi_Credentials.ResourceUrl ,PowerBi_Credentials.ClientID ,PowerBi_Credentials.GrantType ,PowerBi_Credentials.Scope ,PowerBi_Credentials.LoginAddress ,PowerBi_Credentials.ClientSecret FROM dbo.PowerBi INNER JOIN dbo.PowerBi_Credentials ON PowerBi.PowerBi_Cred_ID = PowerBi_Credentials.ID WHERE PowerBi.ID = 1";

        public string GetAccessToken(string resourceUri, string clientID, string grantType, string username, string password, string scope, string clientSecret, string loginAddress)
        {
            StringBuilder body = new StringBuilder();
            body.Append("resource=" + HttpUtility.UrlEncode(resourceUri));
            body.Append("&client_id=" + HttpUtility.UrlEncode(clientID));
            body.Append("&grant_type=" + HttpUtility.UrlEncode(grantType));
            body.Append("&username=" + HttpUtility.UrlEncode(username));
            body.Append("&password=" + HttpUtility.UrlEncode(password));
            body.Append("&scope=" + HttpUtility.UrlEncode(scope));
            body.Append("&client_secret=" + HttpUtility.UrlEncode(clientSecret));

            using (WebClient web = new WebClient())
            {
                web.Headers.Add("client-request-id", Guid.NewGuid().ToString());
                web.Headers.Add("return-client-request-id", "true");

                try
                {
                    string data = web.UploadString(loginAddress, body.ToString());

                    dynamic result = JsonConvert.DeserializeObject(data);
                    try
                    {
                        return result.access_token;
                    }
                    catch
                    { // Log as you want }
                    }
                }
                catch (Exception ex)
                {
                    string error = ex.Message;
                    Response.Redirect("Error.aspx");
                }

                return null;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Timer1.Enabled = true;
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["param"] != null)
                {

                    safe = Server.UrlDecode(Request.QueryString["param"]);
                    safe = safe.Replace(' ', '+');
                    if (safe != null)
                    {
                        string temp = DecryptText(safe, "csd123");
                      string[] tempSplit = temp.Split('?');
                        accountid = tempSplit[0];
                        userID = tempSplit[1];
                        dash = tempSplit[2];
                        //     dash = "1";
                    }

                    conString = ConfigurationManager.ConnectionStrings["conStringNewPortal"].ConnectionString;
                    // code to only run at first page load here 

                    //Get credentials
                    //  string conString2 = "Data Source=www.csdtecmo.com; Initial Catalog=BiDB; user id=CSD; password=CSD";
                    string token = null;
                    try
                    {
                        DataSet dsCred = SqlHelper.ExecuteDataset(conString, CommandType.Text, sqlPowerBiCredentials);

                        string Username = "";
                        string Password = "";
                        string ResourceUrl = "";
                        string ClientID = "";
                        string GrantType = "";
                        string Scope = "";
                        string LoginAddress = "";
                        string ClientSecret = "";
                        foreach (DataRow row in dsCred.Tables[0].Rows)
                        {
                            Username = row["Username"].ToString();
                            Password = row["Password"].ToString();
                            ResourceUrl = row["ResourceUrl"].ToString();
                            ClientID = row["ClientID"].ToString();
                            GrantType = row["GrantType"].ToString();
                            Scope = row["Scope"].ToString();
                            LoginAddress = row["LoginAddress"].ToString();
                            ClientSecret = row["ClientSecret"].ToString();
                        }
                        token = GetAccessToken(ResourceUrl, ClientID, GrantType, Username, Password, Scope, ClientSecret, LoginAddress);
                    }
                    catch (Exception ex)
                    {
                        string error = ex.Message;
                        Response.Redirect("Account Settings Invalid.aspx");
                    }

                    //   string token = GetAccessToken("https://analysis.windows.net/powerbi/api", "7b30b199-09bc-45d2-90c4-a6448f573ecb", "password", "tabusiness@csdtecmo.co.za", "csd@tecmo2", "", "m6FaEIWN0FrUX43cnsJMe3/ZJpwzNqEK3xPOoIoVUFc=", "https://login.windows.net/common/oauth2/token");

                    Session["AccessToken"] = token;

                    //After the redirect above to get rid of code=, Session["authResult"] does not equal null, which means you have an
                    //Access Token. With the Acccess Token, you can call the Power BI Get Reports operation. Get Reports returns information
                    //about a Report, not the actual Report visual. You get the Report visual later with some JavaScript. See postActionLoadReport()
                    //in Default.aspx.
                    if (Session["AccessToken"] != null)
                    {
                        //You need the Access Token in an HTML element so that the JavaScript can load a Report visual into an IFrame.
                        //Without the Access Token, you can not access the Report visual.
                        accessToken.Value = Session["AccessToken"].ToString();

                        //In this sample, you get the first Report. In a production app, you would create a more robost
                        //solution

                        //Get first report.

                        //      GetReport(0);
                        if (!IsPostBack)
                        {
                            //Get BI Info
                            try
                            {
                                string paramlist;
                                    paramlist = accountid + ',' + userID + ',' + dash;
                                    ds = SqlHelper.ExecuteDataset(conString, "Powerbi_ExternalProgram_SP", paramlist);
                                if (ds != null && ds.Tables[0].Rows.Count != 0)
                                {
                                    if(dash == "0")
                                    {
                                        ddlReports.DataSource = ds;
                                        ddlReports.DataBind();
                                    }
                                    if(dash == "1")
                                    {
                                        ddlReports.Visible = false;
                                        Label1.Visible = false;
                                      
                                        myLink.Visible = false;
                                        embededURL = ds.Tables[0].Rows[0]["Url"].ToString();
                                        embededURL = embededURL.Replace("\n", "");
                                        embededURL = embededURL.Replace("\r", "");
                                    }
                                }
                                else
                                {
                                    ddlReports.Items.Add("No Reports Found");
                                    ddlReports.Items[0].Value = "";
                                }

                            }
                            catch (Exception ex)
                            {
                                string error = ex.Message;
                                Response.Redirect("error.aspx");

                            }
                        }
                    }

                }
                else
                {
                    Response.Redirect("Error.aspx");
                }
            }
            else
            {
                Response.Redirect("Error.aspx");
            }

        }

        //Get a Report. In this sample, you get the first Report.


        public void GetAuthorizationCode()
        {
            //NOTE: Values are hard-coded for sample purposes.
            //Create a query string
            //Create a sign-in NameValueCollection for query string
            var @params = new NameValueCollection
            {
                //Azure AD will return an authorization code. 
                {"response_type", "code"},

                //Client ID is used by the application to identify themselves to the users that they are requesting permissions from. 
                //You get the client id when you register your Azure app.
                {"client_id", Settings.Default.ClientID},

                //Resource uri to the Power BI resource to be authorized
                //The resource uri is hard-coded for sample purposes
                {"resource", Properties.Settings.Default.PowerBiAPI},

                //After app authenticates, Azure AD will redirect back to the web app. In this sample, Azure AD redirects back
                //to Default page (Default.aspx).
                { "redirect_uri", Settings.Default.RedirectUri}
            };

            //Create sign-in query string
            var queryString = HttpUtility.ParseQueryString(string.Empty);
            queryString.Add(@params);

            //Redirect to Azure AD Authority
            //  Authority Uri is an Azure resource that takes a client id and client secret to get an Access token
            //  QueryString contains 
            //      response_type of "code"
            //      client_id that identifies your app in Azure AD
            //      resource which is the Power BI API resource to be authorized
            //      redirect_uri which is the uri that Azure AD will redirect back to after it authenticates

            //Redirect to Azure AD to get an authorization code
            Response.Redirect(String.Format(Properties.Settings.Default.AADAuthorityUri + "?{0}", queryString));
        }

        public string GetAccessToken(string authorizationCode, string clientID, string clientSecret, string redirectUri)
        {
            //Redirect uri must match the redirect_uri used when requesting Authorization code.
            //Note: If you use a redirect back to Default, as in this sample, you need to add a forward slash
            //such as http://localhost:13526/

            // Get auth token from auth code       
            TokenCache TC = new TokenCache();

            //Values are hard-coded for sample purposes
            string authority = Properties.Settings.Default.AADAuthorityUri;
            AuthenticationContext AC = new AuthenticationContext(authority, TC);
            ClientCredential cc = new ClientCredential(clientID, clientSecret);

            //Set token from authentication result
            return AC.AcquireTokenByAuthorizationCode(
                authorizationCode,
                new Uri(redirectUri), cc).AccessToken;
        }
        public byte[] AES_Decrypt(byte[] bytesToBeDecrypted, byte[] passwordBytes)
        {
            byte[] decryptedBytes = null;

            // Set your salt here, change it to meet your flavor:
            // The salt bytes must be at least 8 bytes.
            byte[] saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged AES = new RijndaelManaged())
                {
                    AES.KeySize = 256;
                    AES.BlockSize = 128;

                    var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);
                    AES.Key = key.GetBytes(AES.KeySize / 8);
                    AES.IV = key.GetBytes(AES.BlockSize / 8);

                    AES.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(ms, AES.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeDecrypted, 0, bytesToBeDecrypted.Length);
                        cs.Close();
                    }
                    decryptedBytes = ms.ToArray();
                }
            }

            return decryptedBytes;
        }
        public string DecryptText(string input, string password)
        {
            try
            {


                // Get the bytes of the string
                byte[] bytesToBeDecrypted = Convert.FromBase64String(input);
                byte[] passwordBytes = Encoding.UTF8.GetBytes(password);
                passwordBytes = SHA256.Create().ComputeHash(passwordBytes);

                byte[] bytesDecrypted = AES_Decrypt(bytesToBeDecrypted, passwordBytes);

                string result = Encoding.UTF8.GetString(bytesDecrypted);

                return result;
            }
            catch
            {
                Response.Redirect("error.aspx");
                return null;
            }
        }
        protected void Timer1_Tick(object sender, EventArgs e)
        {

        }

        //Power BI Reports used to deserialize the Get Reports response.
        public class PBIReports
        {
            public PBIReport[] value { get; set; }
        }
        public class PBIReport
        {
            public string id { get; set; }
            public string name { get; set; }
            public string webUrl { get; set; }
            public string embedUrl { get; set; }
        }
    }
}